package mailjet

import (
	"fmt"
	"log"

	"github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/logger"
	"gitlab.com/barcos.co/gocore/models/request"
)

func (client *MailjetClient) SendVerifyEmail(d request.AuthRegisterMessagingPayload) (bool, errors.Error) {

	sent := false
	someErr := errors.NewError(nil)

	if client.mailjetClient != nil {
		maildata := client.getVerifyTemplate(d.Email, d.FirstName, d.LastName, d.VerificationLink)
		res, err := client.mailjetClient.SendMailV31(maildata)
		if err != nil {
			logger.FLogInfof(log.Default().Writer(), prefix, "%s\n", err.Error())
			someErr.SetInternalServerError(err)
			return sent, someErr
		}
		sent = analizeResponse(res, d.Email)
	}
	return sent, someErr.SetNilError()
}

func (client *MailjetClient) SendResetPasswordEmail(
	d request.AuthResetPasswordEmailMessagingPayload) (bool, errors.Error) {

	sent := false
	someErr := errors.NewError(nil)

	if client.mailjetClient != nil {
		maildata := client.getPasswordResetTemplate(d.Email, d.FirstName, d.LastName, d.VerificationLink)
		res, err := client.mailjetClient.SendMailV31(maildata)
		if err != nil {
			logger.FLogInfof(log.Default().Writer(), prefix, "%s\n", err.Error())
			someErr.SetInternalServerError(err)
			return sent, someErr
		}
		sent = analizeResponse(res, d.Email)
	}

	return sent, someErr.SetNilError()
}

func (client *MailjetClient) getVerifyTemplate(
	email string, name string, lastname string, verifyLink string) *mailjet.MessagesV31 {
	messagesInfo := []mailjet.InfoMessagesV31{{
		From:     client.getFromField(),
		To:       client.getToField(email, name),
		Subject:  "Welcome to the SOS Portal",
		TextPart: "Please complete your registration",
		HTMLPart: fmt.Sprintf("<h3>Dear %s %s, welcome to the SOS Portal by Dataplan!</h3><br />May the shipping force be with you!<br />%s", name, lastname, verifyLink),
	}}
	return &mailjet.MessagesV31{Info: messagesInfo}
}

func (client *MailjetClient) getPasswordResetTemplate(
	email string, name string, lastname string, resetPasswordLink string) *mailjet.MessagesV31 {
	messagesInfo := []mailjet.InfoMessagesV31{{
		From:     client.getFromField(),
		To:       client.getToField(email, name),
		Subject:  "Reset your password",
		TextPart: "Please complete the password reset",
		HTMLPart: fmt.Sprintf("<h3>Dear %s %s, you can change you password for the SOS Portal by clicking the following link!</h3><br />May the shipping force be with you!<br />%s", name, lastname, resetPasswordLink),
	}}
	return &mailjet.MessagesV31{Info: messagesInfo}
}
