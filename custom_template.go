package mailjet

type CustomMailTemplate struct {
	templateID   int
	subject      string
	senderEmail  string
	senderName   string
	reciverEmail string
	reciverName  string
	variables    map[string]interface{}
}

func NewCustomMailTemplate() *CustomMailTemplate {
	return &CustomMailTemplate{}
}

func (t *CustomMailTemplate) GetTemplateID() int {
	return t.templateID
}

func (t *CustomMailTemplate) SetTemplateID(id int) {
	t.templateID = id
}

func (t *CustomMailTemplate) GetSubject() string {
	return t.subject
}

func (t *CustomMailTemplate) SetSubject(subject string) {
	t.subject = subject
}

func (t *CustomMailTemplate) GetSenderEmail() string {
	return t.senderEmail
}

func (t *CustomMailTemplate) SetSenderEmail(email string) {
	t.senderEmail = email
}

func (t *CustomMailTemplate) GetSenderName() string {
	return t.senderName
}

func (t *CustomMailTemplate) SetSenderName(name string) {
	t.senderName = name
}

func (t *CustomMailTemplate) GetReciverEmail() string {
	return t.reciverEmail
}

func (t *CustomMailTemplate) SetReciverEmail(email string) {
	t.reciverEmail = email
}

func (t *CustomMailTemplate) GetReciverName() string {
	return t.reciverName
}

func (t *CustomMailTemplate) SetReciverName(name string) {
	t.reciverName = name
}

func (t *CustomMailTemplate) GetVariables() map[string]interface{} {
	return t.variables
}

func (t *CustomMailTemplate) SetVariables(v map[string]interface{}) {
	t.variables = v
}
