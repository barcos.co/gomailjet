package mailjet

import (
	"log"

	"github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/logger"
	"golang.org/x/text/language"
)

type MailjetTemplateInfo struct {
	Language   language.Tag
	TemplateID int64
}

type MailjetTemplate struct {
	Info []MailjetTemplateInfo
}

func (client *MailjetClient) SendEmailWithTemplate(
	t interfaces.IMessagingServiceTemplate) (bool, errors.Error) {

	var someErr errors.Error
	sent := false

	message := mailjet.InfoMessagesV31{
		From: &mailjet.RecipientV31{
			Email: t.GetSenderEmail(), Name: t.GetSenderName(),
		},
		To: &mailjet.RecipientsV31{
			mailjet.RecipientV31{Email: t.GetReciverEmail(), Name: t.GetReciverName()},
		},
		TemplateID:       t.GetTemplateID(),
		TemplateLanguage: true,
		Subject:          t.GetSubject(),
		Variables:        t.GetVariables(),
	}
	messagesInfo := []mailjet.InfoMessagesV31{}
	messagesInfo = append(messagesInfo, message)
	messages := mailjet.MessagesV31{Info: messagesInfo}
	res, err := client.mailjetClient.SendMailV31(&messages)
	if err != nil {
		logger.FLogInfof(log.Default().Writer(), prefix, "%s\n", err.Error())
		someErr.SetInternalServerError(err)
	} else {
		logger.FLogInfo(log.Default().Writer(), prefix, res.ResultsV31)
	}
	sent = analizeResponse(res, t.GetReciverEmail())
	return sent, someErr.SetNilError()
}

var RegisterTemplate MailjetTemplate = MailjetTemplate{
	Info: []MailjetTemplateInfo{
		{Language: language.Spanish, TemplateID: 2956938},
		{Language: language.English, TemplateID: 2956938},
	},
}

var registerTemplateMatcher = language.NewMatcher(RegisterTemplate.GetAcceptedLanguages())

func (t *MailjetTemplate) GetAcceptedLanguages() []language.Tag {
	arr := []language.Tag{}
	for index := range t.Info {
		if t.Info != nil && t.Info[index].Language.String() != "" {
			arr = append(arr, t.Info[index].Language)
		}
	}
	return arr
}

func (t *MailjetTemplate) FindTemplateInfo(lan language.Tag) (bool, MailjetTemplateInfo) {

	info := MailjetTemplateInfo{Language: language.English, TemplateID: 0}
	_, _, c := registerTemplateMatcher.Match(lan)

	if t.Info != nil && len(t.Info) > 0 {
		info = t.Info[0]
	}

	if c == language.No || c == language.Low {
		return false, info
	}

	for index := range t.Info {
		if lan.String() == t.Info[index].Language.String() {
			info = t.Info[index]
			break
		}
	}

	return true, info
}
