package mailjet

import (
	"log"
	"os"

	"gitlab.com/barcos.co/gocore/logger"
)

type MailjetConfigs struct {
	apiKey    string
	apiSecret string
}

var mailjetConfigs *MailjetConfigs
var mailjetSenderEmail string
var mailjetSenderName string

const (
	MAILJET_API_KEY_ENV      = "MAILJET_API_KEY"
	MAILJET_SECRET_ENV       = "MAILJET_SECRET"
	MAILJET_SENDER_EMAIL_ENV = "MAILJET_SENDER_EMAIL"
	MAILJET_SENDER_NAME_ENV  = "MAILJET_SENDER_NAME"
)

func (s *MailjetConfigs) GetAPIKey() string {
	return s.apiKey
}

func (s *MailjetConfigs) GetAPISecret() string {
	return s.apiSecret
}

func LoadMailjetConfigs() {
	GetMailjetConfigs()
}

func GetMailjetConfigs() MailjetConfigs {
	if mailjetConfigs == nil {
		c := readSystemConfigs()
		mailjetConfigs = &c
		mailjetConfigs.display()
	}
	return *mailjetConfigs
}

func GetMailjetSender() (string, string) {
	email, name := readSender()

	if mailjetSenderEmail == "" {
		mailjetSenderEmail = email
	}

	if mailjetSenderName == "" {
		mailjetSenderName = name
	}
	return mailjetSenderEmail, mailjetSenderName
}

func readSystemConfigs() MailjetConfigs {
	apiKey := os.Getenv(MAILJET_API_KEY_ENV)
	apiSecret := os.Getenv(MAILJET_SECRET_ENV)
	return MailjetConfigs{apiKey: apiKey, apiSecret: apiSecret}
}

func readSender() (string, string) {
	email := os.Getenv(MAILJET_SENDER_EMAIL_ENV)
	name := os.Getenv(MAILJET_SENDER_NAME_ENV)
	return email, name
}

func (s *MailjetConfigs) display() {
	prefix := "Mailjet Config Read"
	logger.FLogInfof(log.Default().Writer(), prefix, "APIKey    \t\t --> %s \n", s.GetAPIKey())
	logger.FLogInfof(log.Default().Writer(), prefix, "APISecret \t\t --> %s \n\n", s.GetAPISecret())

}
