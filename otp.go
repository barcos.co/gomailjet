package mailjet

import (
	"fmt"
	"log"
	"strings"

	"github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/logger"
	"gitlab.com/barcos.co/gocore/models/request"
)

const (
	OtpFirstName  = "OTP_FIRST_NAME"
	OtpLastName   = "OTP_LAST_NAME"
	OtpVerifyLink = "OTP_VERIFY_LINK"
)

var otpSubject = "OPT Access Link"
var htmlEmail = fmt.Sprintf(`
	<h2>Hola %s %s!</h2>
	<br />
	<p>Accede con el siguiente link a tu perfil de SCOBA SkinCare Center!</p>
	<br />
	%s
	<br />
	<p>Hecho en Cartagena con ❤️ <a href='https://barcos.co'>by Helmer Barcos</a></p>
`, OtpFirstName, OtpLastName, OtpVerifyLink)

type OTPParams struct {
	Subject string
	HTML    string
}

func (c *MailjetClient) GetOPTParams() OTPParams {
	return OTPParams{Subject: otpSubject, HTML: htmlEmail}
}

func (c *MailjetClient) SetOPTParams(params OTPParams) {
	if params.Subject != "" {
		otpSubject = params.Subject
	}

	if params.HTML != "" {
		htmlEmail = params.HTML
	}
}

func (client *MailjetClient) SendOTPEmail(
	d request.OTPAuthLoginMessagingPayload) (bool, errors.Error) {

	sent := false
	someErr := errors.NewError(nil)

	if client.mailjetClient != nil {
		maildata := client.getOTPEmail(d.Email, d.FirstName, d.LastName, d.OTPLink)
		res, err := client.mailjetClient.SendMailV31(maildata)
		if err != nil {
			logger.FLogInfof(log.Default().Writer(), prefix, "%s\n", err.Error())
			someErr.SetInternalServerError(err)
			return sent, someErr
		}

		sent = analizeResponse(res, d.Email)
	}
	return sent, someErr.SetNilError()
}

func (c *MailjetClient) getOTPEmail(
	email string, firstName string, lastname string, verifyLink string) *mailjet.MessagesV31 {

	s := strings.Replace(htmlEmail, OtpFirstName, firstName, -1)
	s = strings.Replace(s, OtpLastName, lastname, -1)
	s = strings.Replace(s, OtpVerifyLink, verifyLink, -1)

	messagesInfo := []mailjet.InfoMessagesV31{{
		From:     c.getFromField(),
		To:       c.getToField(email, fmt.Sprintf("%s %s", firstName, lastname)),
		Subject:  c.GetOPTParams().Subject,
		HTMLPart: s,
	}}
	return &mailjet.MessagesV31{Info: messagesInfo}
}
