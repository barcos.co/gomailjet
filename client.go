package mailjet

import (
	"strings"

	"github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/barcos.co/gocore/errors"
)

var prefix = "MailjetClient Event"

type MailjetClient struct {
	configs            MailjetConfigs
	defaultSenderEmail string
	defaultSenderName  string
	mailjetClient      *mailjet.Client
}

func NewMailjetClient() MailjetClient {
	c := MailjetClient{}
	c.configs = GetMailjetConfigs()
	email, name := GetMailjetSender()

	c.defaultSenderEmail = email
	c.defaultSenderName = name
	c.mailjetClient = mailjet.NewMailjetClient(c.configs.GetAPIKey(), c.configs.GetAPISecret())

	return c
}

func (c *MailjetClient) SetMailProviderAPIKey(apikey string) {
	c.configs.apiKey = apikey
}

func (c *MailjetClient) SetMailProviderSecret(secret string) {
	c.configs.apiSecret = secret
}

func (c *MailjetClient) GetDefaultSenderEmail() string {
	return c.defaultSenderEmail
}

func (c *MailjetClient) SetDefaultSenderEmail(sender string) {
	c.defaultSenderEmail = sender
}

func (c *MailjetClient) GetDefaultSenderName() string {
	return c.defaultSenderName
}

func (c *MailjetClient) SetDefaultSenderName(name string) {
	c.defaultSenderName = name
}

func (client *MailjetClient) SendEmail(email string) (bool, errors.Error) {
	return false, errors.NewError(nil)
}

func (client *MailjetClient) SendSMS(number string) (bool, errors.Error) {
	return false, errors.NewError(nil)
}

func (client *MailjetClient) SendIOSPushNotification(deviceId string) (bool, errors.Error) {
	return false, errors.NewError(nil)
}

func (client *MailjetClient) getFromField() *mailjet.RecipientV31 {
	return &mailjet.RecipientV31{
		Email: client.GetDefaultSenderEmail(),
		Name:  client.GetDefaultSenderName(),
	}
}

func (client *MailjetClient) getToField(email string, name string) *mailjet.RecipientsV31 {
	return &mailjet.RecipientsV31{
		mailjet.RecipientV31{Email: email, Name: name},
	}
}

func analizeResponse(res *mailjet.ResultsV31, email string) bool {
	sent := false
	if res != nil && len(res.ResultsV31) > 0 {
		for index := range res.ResultsV31 {
			status := strings.TrimSpace(res.ResultsV31[index].Status)
			if toArr := res.ResultsV31[index].To; len(toArr) > 0 && status == "success" {
				for index2 := range toArr {
					if toArr[index2].Email == email {
						sent = true
					}
				}
			}
		}
	}

	return sent
}
